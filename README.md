안녕하세요.

카카오 카페파트 입니다.

면접에 참고하기 위한 위한 코딩테스트 문제 보내드립니다.

아래 2개 문제중 최소한 1개이상을 풀어 36시간 이내에 회신해 주세요.

회신주소: kit.t@kakaocorp.com



문제 1)

RBAC 개념이 적용된 Java Web Application을 구현해 주세요.



문제 2)

아래 첨부파일의 README.md 를 참고하여 리펙토링을 진행해 주세요.

https://drive.google.com/file/d/0B_P4j84T0aMNN1Q1ejUzMDVnOTA/view?usp=sharing



감사합니다.

카카오 드림

---


## Overview
레거시 코드(CafeCategoryMgr.java)에 대한 리펙토링 과제

## 주요 클래스/패키지
<pre>
daum.cafe.service.svccafeinfo.manager.CafeCategoryMgr ---- 리펙토링 대상
daum.cafe.service.svccafeinfo.dao.IcategoryHandler ----- DAO
daum.cafe.service.svccafeinfo.entity.Tcategory ---- DB Entity 클래스
daum.cafe.service.svccafeinfo.model.CafeCategoryNode ---- View Model 클래스
clients/* --- 리펙토링 대상을 사용하는 클라이언트 코드들
</pre>

## 문제점
리펙토링 대상인 CafeCategoryMgr 클래스는 아주 오래전에 누군가 작성한 코드로 유닛 테스트도 없고 읽기가 불편하며 사용하지 않는 로직이나 비효율적인 부분이 있습니다. 게다가 동시성 문제도 있습니다.

## 진행방법
git 저장소를 생성하여 주시고 리펙토링 과정을 살펴볼 수 있도록 커밋을 나눠주세요.

## DB의존성
예제환경에서는 DB에 연결할 수 없을 것입니다

[json 파일](src/test/resources/category.json)과 [GsonTest.java](src/test/java/com/google/gson/GsonTest.java)를 참고하여 테스트에 활용해 주세요